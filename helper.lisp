(in-package :sodium)

(defun array-to-lisp (a length)
  (let ((la (make-array length :element-type '(unsigned-byte 8))))
    (loop for i from 0 below length
       do (setf (aref la i) (cffi:mem-aref a :unsigned-char i)))
    la))

(defun lisp-to-array (a)
  (let ((arr (cffi:foreign-alloc :unsigned-char :initial-contents a)))
    arr))

(defmacro with-lisp-array-pointer ((var array) &rest body)
  `(if ,array
       (cffi:with-foreign-pointer (,var (length ,array))
         (loop for i from 0 below (length ,array)
            do (setf (aref ,array i) (cffi:mem-aref ,var :unsigned-char i)))
         ,@body)
       (let ((,var (cffi:null-pointer)))
         ,@body)))

(defun array-to-string (a)
  (with-output-to-string (os)
    (loop for c across a
       do (format os "~x" c))))

;;; Cryptographically-secure random number generation

(defun random-int (&optional below)
  "Generates a random, uniformly-distributed 32-bit integer below below"
  (if below
      (randombytes-uniform below)
      (randombytes-random)))

(defun random-array (a)
  "Fills a lisp array with random bytes"
  (cffi:with-foreign-pointer (c-arr (length a))
    (randombytes-buf c-arr (length a))
    (array-to-lisp c-arr (length a))))

(defun random-bytes (n)
  "Creates an array filled with n random bytes"
  (random-array (make-array n :element-type '(unsigned-byte 8))))

;;; Generic hashing

(defun random-hash-key ()
  "Generate a random hash key suitable for the generic-hash methods."
  (random-bytes (crypto-generichash-keybytes)))

(defgeneric generic-hash (a &key key string-output-p)
  (:documentation
   #.(format nil "Hashes an array of (unsigned-byte 8) or a string.~@
                  ~@
                  key (if provided) must be an array of (unsigned-byte 8)~@
                  or a string and should have length between~@
                  (crypto-generichash-bytes-min) and~@
                  (crypto-generichash-bytes-max)~@
                  See: (describe 'sodium:random-hash-key)~@
                  See: https://download.libsodium.org/doc/hashing/generic_hashing.html~@
                  ~@
                  string-output-p if T will cause the methods to return the hash in~@
                  hex-encoded string form.")))

(defmethod generic-hash ((a array) &key key string-output-p)
  ;; Convert the key from string if necessary
  (when (typep key 'string)
    (setf key (flexi-streams:string-to-octets key)))

  (let* ((byte-count (crypto-generichash-bytes))
         (out
          (cffi:with-foreign-pointer (out byte-count)
            (with-lisp-array-pointer (in a)
              (with-lisp-array-pointer (key-arr key)
                (crypto-generichash out byte-count
                                    in (length a)
                                    key-arr (length key))
                (array-to-lisp out byte-count))))))

    (if string-output-p
        (array-to-string out)
        out)))

(defmethod generic-hash ((s string) &key key string-output-p)
  (generic-hash (flexi-streams:string-to-octets s)
                :key key :string-output-p string-output-p))

(defmacro with-stateful-generic-hash ((state &key key string-output-p) &rest body)
  #.(format nil "Perform a stateful hashing operation.~@
                 ~@
                 key (if provided) must be an array of (unsigned-byte 8)~@
                 or a string and should have length between~@
                 (crypto-generichash-bytes-min) and~@
                 (crypto-generichash-bytes-max)~@
                 See: (describe 'sodium:random-hash-key)~@
                 ~@
                 string-output-p if T will cause the methods to return the hash in~@
                 hex-encoded string form.~@
                 ~@
                 Within the body, calls to generic-hash-update should be made to add~@
                 chunks of data to the final hash.~@
                 See: https://download.libsodium.org/doc/hashing/generic_hashing.html")
  (let ((keysym (gensym "key"))
        (k (gensym "k"))
        (outbytes (gensym "outbytes")))
    `(let* ((,outbytes (crypto-generichash-bytes))
            (,keysym ,key))
       (when (typep ,keysym 'string)
         (setf ,keysym (flexi-streams:string-to-octets ,keysym)))
       (cffi:with-foreign-pointer (,state (crypto-generichash-statebytes))
         (if ,keysym
             (with-lisp-array-pointer (,k ,keysym)
               (crypto-generichash-init ,state ,k (length ,keysym) ,outbytes)
               ,@body)
             (progn
               (crypto-generichash-init ,state (cffi:null-pointer) 0 ,outbytes)
               ,@body))
         (cffi:with-foreign-pointer (out ,outbytes)
           (crypto-generichash-final ,state out ,outbytes)
           (array-to-lisp out ,outbytes))))))

(defgeneric generic-hash-update (state arr)
  (:documentation
   #.(format nil "For use within with-stateful-generic-hash.~@
                  Adds a chunk of data to be hashed.")))

(defmethod generic-hash-update (state (arr array))
  (with-lisp-array-pointer (c-arr arr)
    (crypto-generichash-update state c-arr (length arr))))

(defmethod generic-hash-update (state (arr string))
  (generic-hash-update state (flexi-streams:string-to-octets arr)))


;;; Short hashing

(defun random-shorthash-key ()
  "Generate a random hash key suitable for the short-hash methods."
  (random-bytes (crypto-shorthash-keybytes)))

(defgeneric short-hash (a key)
  (:documentation
   #.(format nil "Hashes an array of (unsigned-byte 8) or a string.~@
                  \"Sodium provides the crypto_shorthash() function,~@
                  which outputs short but unpredictable (without knowing~@
                  the secret key) values suitable for picking a list in a~@
                  hash table for a given key.\"~@
                  ~@
                  key must be an array of (unsigned-byte 8) or a string~@
                  and must have length of (crypto-shorthash-keybytes)~@
                  See: (describe 'sodium:random-shorthash-key)~@
                  See: https://download.libsodium.org/doc/hashing/short-input_hashing.html~@
                  ~@
                  The output is a 64-bit integer.")))

(defmethod short-hash ((a array) (key array))
  (when (typep key 'string)
    (setf key (flexi-streams:string-to-octets key)))

  (let* ((byte-count (crypto-shorthash-bytes))
         (out
          (cffi:with-foreign-pointer (out byte-count)
            (with-lisp-array-pointer (in a)
              (with-lisp-array-pointer (key-arr key)
                (crypto-shorthash out
                                  in (length a)
                                  key-arr)
                (array-to-lisp out byte-count))))))
    (reduce (lambda (acc b) (+ (ash acc 8) b)) out)))

(defmethod short-hash ((a string) (key array))
  (short-hash (flexi-streams:string-to-octets a) key))
