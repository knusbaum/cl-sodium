(in-package :sodium)

;; Random Number Generator
(cffi:defcfun ("randombytes_random" randombytes-random) :uint32)
(cffi:defcfun ("randombytes_uniform" randombytes-uniform) :uint32
  (upper-bound :uint32))
(cffi:defcfun ("randombytes_buf" randombytes-buf) :void
  (buf :pointer)
  (size size))

;; Generic Hash
(cffi:defcfun ("crypto_generichash_bytes_min" crypto-generichash-bytes-min) size)
(cffi:defcfun ("crypto_generichash_bytes_max" crypto-generichash-bytes-max) size)
(cffi:defcfun ("crypto_generichash_bytes" crypto-generichash-bytes) size)
(cffi:defcfun ("crypto_generichash_keybytes_min" crypto-generichash-keybytes-min) size
  "The minimum size in bytes that a key for a generic hash should be")
(cffi:defcfun ("crypto_generichash_keybytes_max" crypto-generichash-keybytes-max) size
  "The maximum size in bytes that a key for a generic hash should be")
(cffi:defcfun ("crypto_generichash_keybytes" crypto-generichash-keybytes) size
  "The recommended size in bytes for a key for a generic hash")

(cffi:defcfun ("crypto_generichash" crypto-generichash) :int
  (out :pointer)
  (outlen size)
  (in :pointer)
  (inlen size)
  (key :pointer)
  (keylen size))

(cffi:defcfun ("crypto_generichash_init" crypto-generichash-init) :int
  (state :pointer)
  (key :pointer)
  (keylen size)
  (outlen size))

(cffi:defcfun ("crypto_generichash_update" crypto-generichash-update) :int
  (state :pointer)
  (in :pointer)
  (inlen size))

(cffi:defcfun ("crypto_generichash_final" crypto-generichash-final) :int
  (state :pointer)
  (out :pointer)
  (outlen size))

(cffi:defcfun ("crypto_generichash_statebytes" crypto-generichash-statebytes) size)

;; Short Hash
(cffi:defcfun ("crypto_shorthash_bytes" crypto-shorthash-bytes) size)
(cffi:defcfun ("crypto_shorthash_keybytes" crypto-shorthash-keybytes) size
  "The required size in bytes for a shorthash key")
(cffi:defcfun ("crypto_shorthash" crypto-shorthash) :int
  (out :pointer)
  (in :pointer)
  (inlen size)
  (k :pointer))
