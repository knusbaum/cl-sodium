cl-sodium - A Common Lisp Interface to Libsodium
=================================

### Cryptographically Secure Random Numbers
```
random-array (a (simple-array (unsigned-byte 8))) -> (simple-array (unsigned-byte 8))

Fills a lisp array with random bytes
```

---

```
random-bytes (n (integer)) -> (simple-array (unsigned-byte 8))

Creates an array filled with `n` random bytes
```

---

```
random-int &optional (below (integer)) -> (integer)

Generates a random, uniformly-distributed 32-bit integer below `below`
```

---

### Generic Hashing
```
random-hash-key -> (array (unsigned-byte 8))

Generate a random hash key suitable for the generic-hash methods.
```

---

```
generic-hash (a (string | (array (unsigned-byte 8)))) &key (key (string | (array (unsigned-byte 8)))) (string-output-p (boolean)) -> ((array (unsigned-byte 8)) | string)

Hashes an array of (unsigned-byte 8) or a string.

key (if provided) must be an array of (unsigned-byte 8)
or a string and should have length between
(crypto-generichash-keybytes-min) and
(crypto-generichash-keybytes-max)
See: (describe 'sodium:random-hash-key)
See: https://download.libsodium.org/doc/hashing/generic_hashing.html

string-output-p if T will cause the methods to return the hash in
hex-encoded string form.
```

---

```
with-stateful-generic-hash (state (symbol)) &key (key (string | (array (unsigned-byte 8)))) (string-output-p (boolean)) &rest body -> ((array (unsigned-byte 8)) | string)

Perform a stateful hashing operation.

key (if provided) must be an array of (unsigned-byte 8)
or a string and should have length between
(crypto-generichash-keybytes-min) and
(crypto-generichash-keybytes-max)
See: (describe 'sodium:random-hash-key)

string-output-p if T will cause the methods to return the hash in
hex-encoded string form.

Within the body, calls to generic-hash-update should be made to add
chunks of data to the final hash.
See: https://download.libsodium.org/doc/hashing/generic_hashing.html
```

---

```
generic-hash-update (state (symbol)) (arr (string | (array (unsigned-byte 8)))) -> nil

For use within with-stateful-generic-hash.
Adds a chunk of data to be hashed.
```

---

### Short Hashes
```
random-shorthash-key -> (array (unsigned-byte 8))

Generate a random hash key suitable for the short-hash methods.
```

---

```
short-hash (a (string | (array (unsigned-byte 8)))) (key (string | (array (unsigned-byte 8)))) -> (integer)

Hashes an array of (unsigned-byte 8) or a string.
"Sodium provides the crypto_shorthash() function,
which outputs short but unpredictable (without knowing
the secret key) values suitable for picking a list in a
hash table for a given key."

key must be an array of (unsigned-byte 8) or a string
and must have length of (crypto-shorthash-keybytes)
See: (describe 'sodium:random-shorthash-key)
See: https://download.libsodium.org/doc/hashing/short-input_hashing.html

The output is a 64-bit integer.
```

---

### Utility Functions
```
crypto-generichash-keybytes-min -> (integer)

The minimum size in bytes that a key for a generic hash should be
```

---

```
crypto-generichash-keybytes-max -> (integer)

The maximum size in bytes that a key for a generic hash should be
```

---

```
crypto-generichash-keybytes -> (integer)

The recommended size in bytes for a key for a generic hash
```

---

```
crypto-shorthash-keybytes -> (integer)

The required size in bytes for a shorthash key
```