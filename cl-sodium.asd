(asdf:defsystem cl-sodium
  :author "Kyle Nusbaum"
  :license "MIT"
  :version "0.0.1"
  :description "cl-sodium provides an interface to libsodium."
  :defsystem-depends-on ("cffi-grovel")
  :depends-on (#:cffi #:flexi-streams)
  :components ((:file "sodium")
               (:cffi-grovel-file "sodium-grovel" :depends-on ("sodium"))
               (:file "sodium-ffi" :depends-on ("sodium"))
               (:file "helper" :depends-on ("sodium"))))
