(defpackage :sodium
  (:use :cl :cffi)
  (:export

   ;; Cryptographically-secure random number generation
   random-array
   random-bytes
   random-int

   ;; Generic hashing
   random-hash-key
   generic-hash
   with-stateful-generic-hash
   generic-hash-update

   ;; Short hashing
   random-shorthash-key
   short-hash

   ;; utility
   crypto-generichash-keybytes-min
   crypto-generichash-keybytes-max
   crypto-generichash-keybytes
   crypto-shorthash-keybytes))


(in-package :sodium)

(eval-when (:load-toplevel)
  (unless (cffi:foreign-symbol-pointer "sodium_init")
    (define-foreign-library libsodium
      (:darwin (:or "libsodium.dylib"))
      (:unix (:or "libsodium.so" "libsodium.so.18"))
      (:windows (:or "libsodium-10.dll"
                     "libsodium.dll"))
      (t (:default "libsodium")))
    (unless (foreign-library-loaded-p 'libsodium)
      (use-foreign-library libsodium))))
